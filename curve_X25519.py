# Based on https://www.rfc-editor.org/rfc/pdfrfc/rfc7748.txt.pdf
# Curve 25519: y^2 = x^3 + 486662 x^2 + x
import os

A = 486662
A24 = int((A - 2) / 4)
P = 2 ** 255 - 19
# Based point constant
G = 9
bytes_num = 32

def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * bytes_num * 8 + int(b)
    return result

def int_to_bytes(value, length):
    result = []
    for i in range(0, length):
        result.append(value >> (i * 8) & 0xff)
    return bytes(bytearray(result))

def cswap(swap, x_2, x_3):
    dummy = swap * ((x_2 - x_3) % P)
    x_2 = x_2 - dummy
    x_3 = x_3 + dummy
    return (x_2, x_3)

def scalar_mul_x25519(k, u):
    x_1 = u
    x_2 = 1
    z_2 = 0
    x_3 = u
    z_3 = 1
    swap = 0
    for t in reversed(range(bytes_num * 8)):
        k_t = (k >> t) & 1
        swap ^= k_t
        x_2, x_3 = cswap(swap, x_2, x_3)
        z_2, z_3 = cswap(swap, z_2, z_3)
        swap = k_t

        A = x_2 + z_2
        A %= P

        AA = A * A
        AA %= P

        B = x_2 - z_2
        B %= P

        BB = B * B
        BB %= P

        E = AA - BB
        E %= P

        C = x_3 + z_3
        C %= P

        D = x_3 - z_3
        D %= P

        DA = D * A
        DA %= P

        CB = C * B
        CB %= P

        x_3 = ((DA + CB) % P)**2
        x_3 %= P

        z_3 = x_1 * (((DA - CB) % P)**2) % P
        z_3 %= P

        x_2 = AA * BB
        x_2 %= P

        z_2 = E * ((AA + (A24 * E) % P) % P)
        z_2 %= P
    
    x_2, x_3 = cswap(swap, x_2, x_3)
    z_2, z_3 = cswap(swap, z_2, z_3)
    # pow(base, exp, mod)
    return (x_2 * pow(z_2, P - 2, P)) % P

def encode_x25519(k):
    k_list = [(b) for b in k]
    # set the three least significant bits of the first byte 
    k_list[0] &= 248
    # set the most significant bit of the last to zero
    k_list[31] &= 127
    # set the second most significant bit of the last byte to 1
    k_list[31] |= 64
    return int.from_bytes(k_list, 'little', signed = False)

# Start at G. Find point n times x-point
def base_point_mult(n):
    return scalar_mul_x25519(n, G)

a_private = os.urandom(bytes_num)
b_private = os.urandom(bytes_num)

print(f"\nAlice private (a):\t", a_private)
print(f"Bob private (b):\t", b_private)

a_private = encode_x25519(a_private)
b_private = encode_x25519(b_private)

# Traditional ECDH: 
a_public = base_point_mult(a_private)
b_public = base_point_mult(b_private)

print("\nAlice public (aG):\t", int_to_bytes(a_public, bytes_num))
print("Bob public (bG):\t", int_to_bytes(b_public, bytes_num))

k_a = scalar_mul_x25519(a_private, b_public) # a(bG)
k_b = scalar_mul_x25519(b_private, a_public) # b(aG)

print("\nAlice shared (a)bG:\t", int_to_bytes(k_a, bytes_num))
print("Bob shared (b)aG:\t" , int_to_bytes(k_b, bytes_num))
