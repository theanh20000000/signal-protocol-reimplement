def gcd(a,b):
    while b != 0:
        a, b = b, a % b
    return a

# def primRoots(modulo):
#     result = []
#     required_set = set(num for num in range (1, modulo) if gcd(num, modulo) == 1)

#     for g in range(1, modulo):
#         actual_set = set(pow(g, powers) % modulo for powers in range (1, modulo))
#         if required_set == actual_set:
#             return g
#             result.append(g)
#     return result

# Alice private key
a = 23
# Bob private key
b = 34
# Server public prime number
p = 121665
# Server public generator
g = 1238498
print("g", g)

ag = g**a%p
print("ag", ag)

bg = g**b%p
print("bg", bg)

agb = ag**b%p
print("agb", agb)

bga = bg**a%p
print("bga", bga)
